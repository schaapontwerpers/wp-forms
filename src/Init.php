<?php

namespace WordPressForms;

use WordPressPluginAPI\Manager;

class Init
{
    private $hooks;

    public function __construct()
    {
        $this->hooks = [
            new RestAPI\Send(),
        ];

        // Run PluginAPI registration
        $manager = new Manager();
        foreach ($this->hooks as $class) {
            $manager->register($class);
        }
    }
}
