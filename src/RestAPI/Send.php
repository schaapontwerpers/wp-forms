<?php

namespace WordPressForms\RestAPI;

use EasyDOM\DOMDocument;
use libphonenumber\PhoneNumberUtil;
use WordPressClassHelpers\Register\RestRoute;

class Send extends RestRoute
{
    private $DOM;

    private $emails = '';

    private $errorResponse;

    private $formBlock;

    private $formId;

    private $headers;

    private $inputs = [];

    private $invalidValues = [];

    private $requiredFields = [];

    private $tableHTML;

    private $url;

    private $values;

    protected $methods = \WP_REST_SERVER::CREATABLE;

    protected function setRoute()
    {
        $this->route = 'forms';
        $this->args = [
            'id' => [
                'required' => true,
                'validate_callback' => function ($param) {
                    return is_string($param);
                },
            ],
            'url' => [
                'required' => true,
                'validate_callback' => function ($param) {
                    return is_string($param);
                },
            ],
            'values' => [
                'required' => true,
                'validate_callback' => function ($param) {
                    return is_array($param);
                },
            ],
        ];
    }

    /**
     * Build the callback
     */
    public function getCallback(\WP_REST_Request $request)
    {
        $response = [];

        $this->formId = $request->get_param('id');
        $this->values = $request->get_param('values');
        $this->url = $request->get_param('url');

        $this->bindValuesToFormData();

        if (count($this->inputs) > 0) {
            $validated = $this->validate();

            if ($validated) {
                $this->buildHTML();

                $this->headers = array(
                    'From: ' . MAIL_FROM_NAME . ' <' . MAIL_FROM . '>',
                    'MIME-Version: 1.0',
                    'Content-Type: text/html; charset=UTF-8',
                );

                if (
                    (array_key_exists(
                        'sendEmail',
                        $this->formBlock['attrs']
                    ) && $this->formBlock['attrs']['sendEmail']) ||
                    !array_key_exists(
                        'sendEmail',
                        $this->formBlock['attrs']
                    )
                ) {
                    $this->sendEmail();
                }

                if (
                    (array_key_exists(
                        'sendConfirmation',
                        $this->formBlock['attrs']
                    ) && $this->formBlock['attrs']['sendConfirmation']) ||
                    !array_key_exists(
                        'sendConfirmation',
                        $this->formBlock['attrs']
                    )
                ) {
                    $this->sendConfirmation();
                }

                do_action(
                    'jabbado_form_submit',
                    $this->inputs,
                    $this->formBlock['attrs']
                );
            }
        }

        if ($this->errorResponse && $this->errorResponse->has_errors()) {
            $response = $this->errorResponse;
        } else {
            $response = new \WP_REST_Response(
                [
                    'code' => 'submit_succesful',
                    'message' => __('Form submit succesful!', 'jabbado'),
                ],
            );
            $response->set_status(201);
        }

        return rest_ensure_response($response);
    }

    /**
     * Get the permission callback
     */
    public function getPermissionCallback(): bool
    {
        return true;
    }

    /**
     * Validate all fields
     */
    private function bindValuesToFormData()
    {
        $pagename = basename(
            untrailingslashit(
                str_replace(FRONTEND_URL, '', $this->url)
            )
        );

        $postTypes = array_values(
            get_post_types(['exclude_from_search' => false])
        );
        array_push($postTypes, 'wp_block');

        $query = new \WP_Query([
            's' => '"formId":"' . $this->formId . '"',
            'posts_per_page' => 1,
            'post_type' => $postTypes,
            'name' => $pagename,
        ]);

        if ($query->have_posts()) {
            $content = $query->posts[0]->post_content;
            $blocks = parse_blocks($content);

            $this->findBlock($blocks);

            if ($this->formBlock) {
                foreach ($this->values as $inputId => $value) {
                    $filteredInputId = str_replace(
                        $this->formId . '-',
                        '',
                        $inputId
                    );

                    $inputBlock = $this->findInput(
                        $this->formBlock['innerBlocks'],
                        $filteredInputId,
                    );

                    if ($inputBlock) {
                        $type = $this->getInputType($inputBlock);
                        $input = [
                            'id' => $inputId,
                            'label' => $inputBlock['attrs']['label'],
                            'type' => $type,
                            'value' => $type !== 'fileupload' ?
                                $this->stripString($value) :
                                $value,
                            'required' => array_key_exists(
                                'isRequired',
                                $inputBlock['attrs']
                            ) ? $inputBlock['attrs']['isRequired'] : true,
                        ];

                        array_push($this->inputs, $input);
                    }
                }
            } else {
                $this->errorResponse = new \WP_Error(
                    'invalid_form_id',
                    __('Invalid form ID.', 'jabbado'),
                    [
                        'status' => 404,
                    ]
                );
            }
        } else {
            $this->errorResponse = new \WP_Error(
                'invalid_form_id',
                __('Invalid form ID.', 'jabbado'),
                [
                    'status' => 404,
                ]
            );
        }
    }

    /**
     * Strip input value
     */
    private function stripString(string $string): string
    {
        $string = strip_tags($string);
        $string = trim($string);
        $string = stripslashes($string);
        $string = htmlspecialchars($string);

        return $string;
    }

    private function findBlock(array $blocks)
    {
        foreach ($blocks as $block) {
            if ($block['blockName'] === 'jabbado/form') {
                $this->formBlock = $block;

                break;
            } elseif (
                array_key_exists('innerBlocks', $block) &&
                count($block['innerBlocks']) > 0
            ) {
                $this->findBlock($block['innerBlocks']);

                if ($this->formBlock) {
                    break;
                }
            }
        }
    }

    private function findInput(array $blocks, string $inputId)
    {
        $input = false;

        foreach ($blocks as $block) {
            if (
                array_key_exists('attrs', $block) &&
                is_array($block['attrs']) &&
                array_key_exists('inputId', $block['attrs']) &&
                $block['attrs']['inputId'] === $inputId
            ) {
                $input = $block;

                break;
            } elseif (
                array_key_exists('innerBlocks', $block) &&
                is_array($block['innerBlocks'])
            ) {
                $input = $this->findInput($block['innerBlocks'], $inputId);

                if ($input) {
                    break;
                }
            }
        }

        return $input;
    }

    /**
     * Get the type of the input
     */
    private function getInputType(array $input): string
    {
        $type = '';

        if ($input['blockName'] === 'jabbado/form-input') {
            $type = array_key_exists('type', $input['attrs']) ?
                $input['attrs']['type'] :
                'text';
        } else {
            $type = str_replace('jabbado/form-', '', $input['blockName']);
        }

        return $type;
    }

    /**
     * Get the checkbox value for in the email
     */
    private function getCheckboxValue(string $value): string
    {
        $translateValue = [
            'on' => __('Checked', 'jabbado'),
            'off' => __('Unchecked', 'jabbado'),
        ];

        return $translateValue[$value];
    }

    /**
     * Build the HTML message
     */
    private function validate(): bool
    {
        foreach ($this->inputs as $input) {
            if (
                $input['required'] &&
                (
                    ($input['value'] === '') ||
                    (($input['type'] === 'checkbox') &&
                        ($input['value'] === 'off'))
                )
            ) {
                array_push($this->requiredFields, $input['id']);
            } elseif ($input['value'] !== '') {
                $this->validateInput(
                    $input['id'],
                    $input['value'],
                    $input['type'],
                );
            }
        }

        $errorsCount = count($this->requiredFields) +
            count($this->invalidValues);

        if ($errorsCount !== 0) {
            $this->errorResponse = new \WP_Error(
                'validation_failed',
                __('Validation on one or more fields has failed.', 'jabbado'),
                [
                    'status' => 400,
                ]
            );

            if (count($this->invalidValues) > 0) {
                $this->errorResponse->add(
                    'value_invalid',
                    __('One or more values are invalid.', 'jabbado'),
                    $this->invalidValues,
                );
            }

            if (count($this->requiredFields) > 0) {
                $this->errorResponse->add(
                    'field_required',
                    __('One or more fields are required.', 'jabbado'),
                    $this->requiredFields,
                );
            }
        }

        return $errorsCount === 0;
    }

    /**
     * Validate a single field
     */
    private function validateInput(
        string $id,
        string $value,
        string $type,
    ) {
        switch ($type) {
            case 'email':
                if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                    array_push($this->invalidValues, $id);
                } else {
                    $this->emails .= (strlen($this->emails) > 0) ?
                        ',' . $value :
                        $value;
                }

                break;

            case 'tel':
                $phoneUtil = PhoneNumberUtil::getInstance();
                $phoneNumber = $phoneUtil->parse(
                    $value,
                    str_starts_with($value, '+') ? null : 'NL'
                );

                if (!$phoneUtil->isValidNumber($phoneNumber)) {
                    array_push($this->invalidValues, $id);
                }

                break;

            default:
                break;
        }
    }

    /**
     * Build the HTML message
     */
    private function buildHTML()
    {
        // Build DomDocument
        $this->DOM = new DOMDocument();

        // Fill table with POST data
        $table = $this->createTable();

        // Build DOM message
        $html = $this->DOM->createElement('html');
        $body = $this->DOM->createElement('body');
        $body->appendChild($table);
        $html->appendChild($body);

        // Save DOM to message
        $this->DOM->appendChild($html);

        // Filter hook for adjustability
        $this->tableHTML = apply_filters(
            'jabbado_table_html',
            $this->DOM->saveHTML($table),
            $this->inputs,
        );
    }

    /**
     * Send the form
     */
    private function createTable()
    {
        // Create table element
        $atts = array(
            'rules' => 'all',
            'cellpadding' => '0',
            'width' => '100%',
            'style' => 'max-width: 600px; margin: 0 auto;',
        );
        $table = $this->DOM->generateElement('table', $atts);
        $tbody = $this->DOM->createElement('tbody');
        $table->appendChild($tbody);

        $i = 0;

        foreach ($this->inputs as $input) {
            $value = $input['type'] === 'checkbox' ?
                $this->getCheckboxValue($input['value']) :
                $input['value'];

            $tr = $this->createRow(
                $input['label'],
                $value,
                $i
            );
            $tbody->appendChild($tr);

            $i++;
        }

        // Add referer to table
        $tr = $this->createRow(
            __('URL', 'jabbado'),
            $this->url,
            $i
        );
        $tbody->appendChild($tr);

        return $table;
    }

    /**
     * Create a row for the table
     */
    private function createRow(
        string $label,
        string $value,
        int $i
    ) {
        $backgroundColor = ($i % 2 == 0) ? '#fefefe' : '#fff';

        $atts = array(
            'style' =>
            'color: #000; background-color: ' . $backgroundColor . ';'
        );
        $tr = $this->DOM->generateElement('tr', $atts);

        $atts = array(
            'align' => 'left',
            'style' => 'border-color: transparent; padding: 9px 36px 9px 0;',
            'valign' => 'top'
        );
        $th = $this->DOM->generateElement('th', $atts);
        $tr->appendChild($th);

        $strong = $this->DOM->generateElement(
            'strong',
            null,
            $label
        );
        $th->appendChild($strong);

        $atts = array(
            'align' => 'left',
            'style' => 'border: 0; padding: 9px 0 9px 0; width: 60%;',
            'valign' => 'top'
        );
        $td = $this->DOM->generateElement('td', $atts, $value);
        $tr->appendChild($td);

        return $tr;
    }

    /**
     * Send the e-mail
     */
    private function sendEmail()
    {
        $to = array_key_exists('emailAddress', $this->formBlock['attrs']) ?
            $this->formBlock['attrs']['emailAddress'] :
            get_option('admin_email');
        $subject = array_key_exists(
            'emailSubject',
            $this->formBlock['attrs']
        ) ?
            $this->formBlock['attrs']['emailSubject'] :
            '';

        // Send mail
        $mail = wp_mail(
            $to,
            $subject !== '' ?
                $subject :
                __('New form submission', 'jabbado'),
            $this->tableHTML,
            $this->headers
        );

        if (!$mail) {
            $this->errorResponse = new \WP_Error(
                'mail_failed',
                __('Unable to send mail.', 'jabbado'),
                [
                    'status' => 500,
                ]
            );
        }
    }

    /**
     * Send the confirmation e-mail
     */
    private function sendConfirmation()
    {
        $subject = array_key_exists(
            'confirmationSubject',
            $this->formBlock['attrs']
        ) ?
            $this->formBlock['attrs']['confirmationSubject'] :
            '';

        $text = array_key_exists(
            'confirmationText',
            $this->formBlock['attrs']
        ) ?
            $this->formBlock['attrs']['confirmationText'] :
            '';
        $confirmationText = $text !== '' ?
            $text :
            __('Thanks for filling out our form!', 'jabbado');
        $confirmationText = apply_filters(
            'jabbado_confirmation_email',
            $confirmationText,
            $this->inputs
        );

        // Send mail
        $mail = wp_mail(
            $this->emails,
            $subject !== '' ?
                $subject :
                __('Confirmation of form submission', 'jabbado'),
            $confirmationText,
            $this->headers
        );

        if (!$mail) {
            $this->errorResponse = new \WP_Error(
                'mail_failed',
                __('Unable to send mail.', 'jabbado'),
                [
                    'status' => 500,
                ]
            );
        }
    }
}
