<?php
/*
Plugin Name:  WP Forms
Plugin URI:   https://packagist.org/packages/schaapdigitalcreatives/wp-forms
Description:  Form handling when using the SDC Blocks library
Version:      1.0.2
Author:       Schaap Digital Creatives
Author URI:   https://schaapontwerpers.nl/
License:      GNU General Public License
*/

new WordPressForms\Init();
